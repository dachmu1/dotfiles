# Nvim Config
Powerful Nvim setup, for programming, note taking and writting


## Installation
can be easily installed with stow, or just copy and paste to the correct config location

Exmaple
``` bash
    rm -r "$XDG_CONFIG_HOME/nvim" ; mkdir "$XDG_CONFIG_HOME/nvim" && stow -t "$XDG_CONFIG_HOME/nvim" .
```

## Dependencies
- ripgrep
- fd
- codelldb

## Plugins 
They should install automatically thanks to Lazy.nvim

## WIP
- [ ] Find a way to actually flexible and extensible note taking strategy 
- [ ] Find how to handle spelling false positives in lsp (they are a spam in the screen)
- [ ] Find how to handle bibliographies
