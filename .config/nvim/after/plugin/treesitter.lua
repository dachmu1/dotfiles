-- lets Configure Treesitter

require("nvim-treesitter.configs").setup {
    highlight = {
        enable = true,
        disable = {},
	additional_vim_regex_hightlighting = false,
    },
    indent = {
        enable = true,
        disable = { "yml", },
    },
    ensure_installed = {
        "c",
        "cpp",
        "dockerfile",
        "html",
        "java",
        "javascript",
        "typescript",
        "json",
        "latex",
        "lua",
        "python",
        "regex",
        "scss",
        "yaml",
        "query",
        "rust",
        "toml",
        "go"
    },
    auto_install = true,
    rainbow = {
        enable = true,
        -- disable = { "jsx", "cpp" }, list of languages you want to disable the plugin for
        extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
        max_file_lines = nil, -- Do not enable for files with more than n lines, int
        -- colors = {}, -- table of hex strings
        -- termcolors = {} -- table of colour name strings
    },
    playground = {
        enable = true,
    }

}
