local telescope = require('telescope')

local actions = require("telescope.actions")

telescope.setup{
    defaults = {
        vimgrep_arguments = {
            'rg',
            '--color=never',
            '--no-heading',
            '--with-filename',
            '--line-number',
            '--column',
            '--smart-case',
        },
        file_ignore_patterns = {
            ".git/",
            "node_modules",
            ".cache",
            "%.o",
            "%.a",
            "%.out",
            "%.class",
            "%.pdf",
            "%.mkv",
            "%.mp4",
            "%.zip",
            "%.rar",
        },
        color_devicons = true,
        set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
        mappings = {
            i = {
                -- map actions.which_key to <C-h> (default: <C-/>)
                -- actions.which_key shows the mappings for your picker,
                -- e.g. git_{create, delete, ...}_branch for the git_branches picker
                ["<C-h>"] = "which_key"
            }
        }
    },
    pickers = {
        buffers = {
            mappings = {
                i = {
                    ["<c-d>"] = actions.delete_buffer + actions.move_to_top, -- Enables buffer closing from telescope
                }
            }
        },
        live_grep = {
            additional_args = function (opts)
                return {"--hidden"}
            end
        }
    },
    extensions = {
        fzf = {
            fuzzy = true,
            override_generic_sorter = true,
            override_file_sorter = true,
            case_mode = "smart_case",
        }
    }
}

require('telescope').load_extension('fzf')

-- Telescope Remaps--

local builtin = require('telescope.builtin')
vim.api.nvim_set_keymap( "n" ,"<leader>ff", ":Telescope find_files hidden=true<CR>", {noremap = true})
vim.api.nvim_set_keymap( "n" ,"<leader>fg", ":Telescope live_grep<CR>", {noremap = true})
vim.api.nvim_set_keymap( "n" ,"<leader>fb", ":Telescope buffers<CR>", {noremap = true})
vim.api.nvim_set_keymap( "n" ,"<leader>fh", ":Telescope help_tags<CR>", {noremap = true})
vim.api.nvim_set_keymap( "n" ,"<leader>fm", ":Telescope marks<CR>", {noremap = true})
vim.api.nvim_set_keymap( "n" ,"<leader>fq", ":Telescope quickfix<CR>", {noremap = true})
vim.api.nvim_set_keymap( "n" ,"<leader>fk", ":Telescope keymaps<CR>", {noremap = true})
vim.api.nvim_set_keymap( "n" ,"<leader>fs", ":Telescope spell_suggest<CR>", {noremap = true})
vim.api.nvim_set_keymap( "n" ,"<leader>fr", ":Telescope registers<CR>", {noremap = true})
vim.api.nvim_set_keymap( "n" ,"<leader>fa", ":Telescope autocommands<CR>", {noremap = true})
vim.api.nvim_set_keymap( "n" ,"<leader>ft", ":Telescope treesitter<CR>", {noremap = true})
vim.keymap.set('n', '<leader>fcc', builtin.commands, {})
vim.keymap.set('n', '<leader>fch', builtin.command_history, {})
