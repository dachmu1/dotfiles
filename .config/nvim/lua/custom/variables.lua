vim.g.mapleader = " "

vim.opt.nu = true
vim.opt.relativenumber = true

local tab_size = 4
vim.opt.expandtab = true
vim.opt.smartindent = true
vim.opt.tabstop = tab_size
vim.opt.softtabstop = tab_size
vim.opt.shiftwidth = tab_size
vim.opt.foldenable = false

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = true
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.bo.fileformat = "unix" -- Makes sure to use unix format files
vim.o.ignorecase = true -- Makes default searches case insesitive
vim.o.smartcase = true -- When searching, vim will be case insensitive if the search start in lowercase, and case sensitive otherwise.

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.opt.colorcolumn = "120"

vim.o.splitbelow = true  -- Makes default vertical split to go down
vim.o.splitright = true  -- Makes default side split window to the right

vim.o.showcmd = true -- Shows the commands being executed in normal mode

vim.wo.conceallevel = 0 -- Prevents vim from hidding some stuff
vim.o.path = vim.o.path .. "**" -- Allows search down into subfolders

vim.g.notespath = os.getenv("HOME") .. "/sync/Athena/"
