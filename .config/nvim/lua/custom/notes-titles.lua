-- Function to check if a buffer is empty
local function is_buffer_empty(buffer)

    -- Get the lines in the buffer
    local lines = vim.api.nvim_buf_get_lines(buffer, 0, -1, false)
    -- Check if the lines table is empty
    local ntx = next(lines)
    print("Found lines: " .. ntx)
    if ntx <= 1 then
        return true
    else
        return false
    end
end

-- Function to insert the buffer name at the top
local function insert_buffer_name()
    local bufnr = vim.api.nvim_get_current_buf()

    if is_buffer_empty(bufnr) then
        local bufname = vim.api.nvim_buf_get_name(bufnr)

        local title = string.match(bufname, "[^/]+$")

        vim.api.nvim_buf_set_lines(bufnr, 0, 0, false, {"* " .. title})
    end
end

vim.api.nvim_create_autocmd('BufNewFile', {
  pattern = '*.norg',
  callback = function () insert_buffer_name()
  end
})
