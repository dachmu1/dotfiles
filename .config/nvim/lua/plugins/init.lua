return {
    -- #TODO
    -- Note Taking with markdown and zettelkastein

    -- ======================================================--
    -- Image Viewing--=======================================--
    { 'samodostal/image.nvim', dependencies = { 'nvim-lua/plenary.nvim' }, },
    -- { 'edluffy/hologram.nvim', config = function ()
    --         require('hologram').setup{
    --             auto_display = true 
    --         }
    --     end
    -- },
    -- -- ======================================================--
    -- Note Taking --========================================--
    { 'ellisonleao/glow.nvim', config = function() require("glow").setup() end },
    { 'Chaitanyabsprip/present.nvim', config = function() require('present').setup{ } end },
    'jbyuki/venn.nvim',

    -- ======================================================--
    -- Navigation --=========================================--
    { 'nvim-telescope/telescope.nvim',
        dependencies = { {'nvim-lua/plenary.nvim'} }
    },
    { 'nvim-telescope/telescope-fzf-native.nvim',
        build = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build'
    },

    'theprimeagen/harpoon',
    'mbbill/undotree',

    -- ======================================================--
    -- Utils --==============================================--
    -- 'ludovicchabant/vim-gutentags', -- Automatic tag management
    'tpope/vim-commentary', -- Manages comments
    'tpope/vim-repeat', -- Allows comentary and surround work with .
    'tpope/vim-surround', -- Surrounding adjective with ys
    'akinsho/toggleterm.nvim',
    'famiu/bufdelete.nvim',
    'folke/which-key.nvim',
    {
        "windwp/nvim-autopairs",
        config = function() require("nvim-autopairs").setup {} end
    },

    -- ======================================================-- 
    -- Aesthetics --=========================================--
    {
        "mcchrish/zenbones.nvim",
        dependencies = "rktjmp/lush.nvim"
    },
    {
        'hoob3rt/lualine.nvim', -- Provides a status line
        dependencies = {'nvim-tree/nvim-web-devicons', opt = true}
    },
    {
        "folke/zen-mode.nvim",
        config = function()
            require("zen-mode").setup {}
        end
    },
    {
        "folke/twilight.nvim",
        config = function()
            require("twilight").setup {}
        end
    },
    'nvim-tree/nvim-web-devicons',
    'akinsho/bufferline.nvim', dependencies = 'nvim-tree/nvim-web-devicons',
    { 'rose-pine/neovim', as = 'rose-pine' },
    'NvChad/nvim-colorizer.lua',


    -- ======================================================--
    -- TreeSitter --=========================================--
    {'nvim-treesitter/nvim-treesitter', build = { ':TSUpdate' }},
    'nvim-treesitter/playground',
    'p00f/nvim-ts-rainbow', -- Better Bracket visibility

    -- ======================================================--
    -- Git --================================================--
    'f-person/git-blame.nvim', -- Git blame
    'airblade/vim-gitgutter', -- Visual aid for git changes
    'tpope/vim-fugitive', -- Git integration
    { 'lewis6991/gitsigns.nvim', dependencies = { 'nvim-lua/plenary.nvim' } }, -- Git signs in columns
    {
        'nvim-tree/nvim-tree.lua',
        dependencies = {
          'nvim-tree/nvim-web-devicons', -- optional, for file icon
        },
        tag = 'nightly' -- optional, updated every week. (see issue #1193)
    },

    -- ======================================================--
    -- LSP --================================================--
    'folke/neodev.nvim',
    {
        'VonHeikemen/lsp-zero.nvim',
        dependencies = {
            -- LSP Support
            {'neovim/nvim-lspconfig'},
            {'williamboman/mason.nvim'},
            {'williamboman/mason-lspconfig.nvim'},

            -- Autocompletion
            {'hrsh7th/nvim-cmp'},
            {'hrsh7th/cmp-buffer'},
            {'hrsh7th/cmp-path'},
            {'saadparwaiz1/cmp_luasnip'},
            {'hrsh7th/cmp-nvim-lsp'},
            {'hrsh7th/cmp-nvim-lua'},

            -- Snippets
            {'L3MON4D3/LuaSnip'},
            {'rafamadriz/friendly-snippets'},
        }
    },
    'ray-x/lsp_signature.nvim', -- Better signatures
    {
        'glepnir/lspsaga.nvim',
        event = 'BufRead',
        keys = { "<leader>s" },
        config = function()
            require('lspsaga').setup({})
        end,
        dependencies = {
            {"nvim-tree/nvim-web-devicons"},
            --Please make sure you install markdown and markdown_inline parser
            {"nvim-treesitter/nvim-treesitter"}
        },
    },
    -- #TODO REPLS
    -- use { 'hkupty/iron.nvim' } -- Fast execute and REPLS
    -- use { 'michaelb/snipbuild', build = 'bash ./install.sh' }
    -- use { 'jubnzv/mdeval.nvim' }

    -- Automatically set up your configuration after cloning packer.nvim
    -- Put this at the end after all plugins
}
