return {
    "lukas-reineke/indent-blankline.nvim",
    main = "ibl",
    opts = {
        enabled = true,
        indent = { char = "▏" },
        whitespace = { highlight = { "Whitespace", "NonText" } },
        scope = {
            enabled = true,
            show_start = false,
            show_end = false,
            priority = 500,
            include = {
                node_type = { lua = { "return_statement", "table_constructor" } },
            },
            highlight = { "Whitespace", "NonText" },
        },
    },
}

