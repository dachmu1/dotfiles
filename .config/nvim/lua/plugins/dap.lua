return {
    {
        'mfussenegger/nvim-dap', -- Debugger Adapter Protocol
        config = function()
            -- local port = 7820
            local dap = require'dap'
            dap.adapters.codelldb = {
                type = 'server',
                port = "${port}",
                executable = {
                    -- CHANGE THIS to your path!
                    command = 'codelldb',
                    args = {"--port", "${port}"},

                    -- On windows you may have to uncomment this:
                    -- detached = false,
                }
            }

            dap.adapters.gdb = {
                type = "executable",
                command = "gdb",
                args = { "-i", "dap" }
            }

            dap.configurations.cpp = {
                {
                    name = "Launch",
                    type = "codelldb",
                    request = "launch",
                    program = function()
                        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/',
                            'file')
                    end,
                    cwd = '${workspaceFolder}',
                    stopOnEntry = false,
                    args = {},

                    runInTerminal = false
                }
            }

            dap.configurations.c = {
                {
                    name = "Launch",
                    type = "gdb",
                    request = "launch",
                    program = function()
                        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/',
                            'file')
                    end,
                    cwd = '${workspaceFolder}',
                    args = {},
                    stopAtBeginningOfMainSubprogram = false,
                    runInTerminal = false
                }
            }
            dap.configurations.rust = {
                {
                    name = "Launch",
                    type = "codelldb",
                    request = "launch",
                    program = function()
                        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/target/debug/',
                            'file')
                    end,
                    cwd = '${workspaceFolder}',
                    stopOnEntry = false,
                    args = {},

                    runInTerminal = false
                }
            }

            vim.keymap.set('n', '<leader>dc', function() require('dap').continue() end)
            vim.keymap.set('n', '<leader>do', function() require('dap').step_over() end)
            vim.keymap.set('n', '<leader>di', function() require('dap').step_into() end)
            vim.keymap.set('n', '<leader>de', function() require('dap').step_out() end)
            vim.keymap.set('n', '<Leader>db', function() require('dap').toggle_breakpoint() end)
            vim.keymap.set('n', '<Leader>dB', function() require('dap').set_breakpoint() end)
            vim.keymap.set('n', '<Leader>dm', function() require('dap').set_breakpoint(nil, nil, vim.fn.input('Log point message: ')) end)
            vim.keymap.set('n', '<Leader>dr', function() require('dap').repl.open() end)
            vim.keymap.set('n', '<Leader>dl', function() require('dap').run_last() end)
            vim.keymap.set({'n', 'v'}, '<Leader>dh', function()
              require('dap.ui.widgets').hover()
            end)
            vim.keymap.set({'n', 'v'}, '<Leader>dp', function()
              require('dap.ui.widgets').preview()
            end)
            vim.keymap.set('n', '<Leader>df', function()
              local widgets = require('dap.ui.widgets')
              widgets.centered_float(widgets.frames)
            end)
            vim.keymap.set('n', '<Leader>ds', function()
              local widgets = require('dap.ui.widgets')
              widgets.centered_float(widgets.scopes)
            end)
        end,
        lazy = true,
        keys = { "<leader>d" }
    },
    {
        'theHamsta/nvim-dap-virtual-text',
        dependencies = {
            'mfussenegger/nvim-dap',
            'nvim-treesitter/nvim-treesitter',
        },
        config = function ()
            require("nvim-dap-virtual-text").setup {
                enabled = true,                        -- enable this plugin (the default)
                enabled_commands = true,               -- create commands DapVirtualTextEnable, DapVirtualTextDisable, DapVirtualTextToggle, (DapVirtualTextForceRefresh for refreshing when debug adapter did not notify its termination)
                highlight_changed_variables = true,    -- highlight changed values with NvimDapVirtualTextChanged, else always NvimDapVirtualText
                highlight_new_as_changed = false,      -- highlight new variables in the same way as changed variables (if highlight_changed_variables)
                show_stop_reason = true,               -- show stop reason when stopped for exceptions
                commented = false,                     -- prefix virtual text with comment string
                only_first_definition = true,          -- only show virtual text at first definition (if there are multiple)
                all_references = false,                -- show virtual text on all all references of the variable (not only definitions)
                display_callback = function(variable, _buf, _stackframe, _node)
                    return variable.name .. ' = ' .. variable.value
                end,

                -- experimental features:
                virt_text_pos = 'eol',                 -- position of virtual text, see `:h nvim_buf_set_extmark()`
                all_frames = false,                    -- show virtual text for all stack frames not only current. Only works for debugpy on my machine.
                virt_lines = false,                    -- show virtual lines instead of virtual text (will flicker!)
                virt_text_win_col = nil                -- position the virtual text at a fixed window column (starting from the first text column) ,
                -- e.g. 80 to position at column 80, see `:h nvim_buf_set_extmark()`
            }
        end,
        lazy = true,
        keys = { "<leader>d" }

    },
    {
        'rcarriga/nvim-dap-ui', -- Debugger user interface
        dependencies = {"mfussenegger/nvim-dap", "nvim-neotest/nvim-nio"},
        config = function ()
            require'dapui'.setup()

            -- local dap, dapui = require("dap"), require("dapui")
            -- dap.listeners.after.event_initialized["dapui_config"] = function()
            --     dapui.open()
            -- end
            -- dap.listeners.before.event_terminated["dapui_config"] = function()
            --     dapui.close()
            -- end
            -- dap.listeners.before.event_exited["dapui_config"] = function()
            --     dapui.close()
            -- end

            vim.keymap.set("n", "<leader>du", ":lua require'dapui'.toggle()<CR>")

        end,
        lazy = true,
        keys = { "<leader>d" }
    },

}
