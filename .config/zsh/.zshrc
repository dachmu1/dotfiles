#!/bin/sh

# History Management
HISTFILE=~/.cache/zsh/history
HISTSIZE=10000
SAVEHIST=10000

# Exports
export MANPAGER='nvim +Man!'
export EDITOR='nvim'
export TERM='alacritty'
export PATH="$PATH:$HOME/.local/bin/"

#Aliases
alias lf="$HOME/.local/bin/lfub" 

# Basic options (man zshoptions)
setopt nomatch
setopt interactive_comments
stty stop undef
zle_highlight=('paste:none')

# No more beep, no more headache
unsetopt BEEP

# Completion settings
autoload -Uz compinit
zmodload zsh/complist
zstyle ':completion:*' menu select
# zstyle ':completion::complete:lsof:*' menu yes select
_comp_options+=(globdots)		# Include hidden files.


autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search


autoload -Uz colors && colors #Colors 
source "$ZDOTDIR/plug-utils.sh"

# Sources
zsh_add_file "vim-mode.sh"
zsh_add_file "prompt.sh"

# Plugins
zsh_add_plugin "zsh-users/zsh-autosuggestions"
zsh_add_plugin "zsh-users/zsh-syntax-highlighting"
zsh_add_plugin "hlissner/zsh-autopair"

# terraform -install-autocomplete

# FZF 
[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/doc/fzf/examples/completion.zsh ] && source /usr/share/doc/fzf/examples/completion.zsh
[ -f /usr/share/doc/fzf/examples/key-bindings.zsh ] && source /usr/share/doc/fzf/examples/key-bindings.zsh
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f $ZDOTDIR/completion/_fnm ] && fpath+="$ZDOTDIR/completion/"
compinit
