# dotfiles
Simple dotfiles repo
Uses Stow file structure
use with stow <folder>

Exmaple
``` bash
    mkdir ~/.config/awesome/ && stow -t ~/.config/awesome/ awesome
```
``` bash
    rm -r ~/.config/rofi/ ; mkdir ~/.config/rofi/ && stow -t ~/.config/rofi/ rofi
```
``` bash
    rm -r ~/.config/lf/ ; mkdir ~/.config/lf/ && stow -t ~/.config/lf/ lf
    mkdir -p ~/.local/bin/ ; stow -t ~/.local/bin/ scripts
```
``` bash
    rm -r ~/.config/tmux/ ; mkdir ~/.config/tmux/ && stow -t ~/.config/tmux/ tmux
```
``` bash
    rm -r ~/.config/alacritty/ ; mkdir ~/.config/alacritty/ && stow -t ~/.config/alacritty/ alacritty
```
``` bash
    rm -r ~/.config/zsh/ ; mkdir ~/.config/zsh/ && stow -t ~/.config/zsh/ -d zsh zsh
```
``` bash
    rm -r ~/.config/zsh/ ; mkdir ~/.config/zsh/ && stow -t ~/ zsh
```
``` bash
    rm -r ~/.config/zsh/ ; mkdir ~/.config/zsh/ && stow -t ~/ zsh
```
``` bash
    stow -t ~/ Xresources
```
``` bash
    rm -r ~/.config/kitty/ ; mkdir ~/.config/kitty/ && stow -t ~/ kitty
```

# Credits
##lf:
- https://github.com/gokcehan/lf/blob/master/etc/lfrc.example
- https://github.com/cirala/lfimg
- https://github.com/seebye/ueberzug RIP but still working as of now, so yeah

##awesome
- https://github.com/elenapan/dotfiles Great work

##Hyprland
- https://github.com/JaKooLit/HyprLanD/
- https://github.com/prasanthrangan/hyprdots
