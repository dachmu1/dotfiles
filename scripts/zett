#!/bin/sh
# POSIX

generate_id() {
    new_id=$(date '+Z%Y%m%d%H%M%S')
    printf "%s\n" "$new_id"
}

show_help() {
    printf "-i/--id would generate a new zettel Id with format Z%Y%m%d%H%M%S"
}

new_zett() {
    if [ -z $1 ]; then zett_path=$1; zett_path="./"; fi
    new_zett_id=$(generate_id)
    echo "$new_zett_id" > "${zett_path}${new_zett_id}.md"
    "${EDITOR:-vi}" "${zett_path}${new_zett_id}.md"
}

# Reset all variables that might be set
file=
verbose=0 # Variables to be evaluated as shell arithmetic should be initialized to a default or validated beforehand.

while :; do
    case $1 in
        -h|-\?|--help)      # Call a "show_help" function to display a synopsis, then exit.
            show_help
            exit
            ;;
        -i|--id)            # Generate new Id based on date
            generate_id
            exit
            ;;
        -n|--new)            # Generate new Id based on date
            new_zett
            exit
            ;;
        -f|--file)          # Takes an option argument, ensuring it has been specified.
            if [ -n "$2" ]; then
                file=$2
                shift
            else
                printf 'ERROR: "--file" requires a non-empty option argument.\n' >&2
                exit 1
            fi
            ;;
        --file=?*)
            file=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --file=)         # Handle the case of an empty --file=
            printf 'ERROR: "--file" requires a non-empty option argument.\n' >&2
            exit 1
            ;;
        -v|--verbose)
            verbose=$((verbose + 1)) # Each -v argument adds 1 to verbosity.
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: If no more options then break out of the loop.
            break
    esac

    shift
done

# if --file was provided, open it for writing, else duplicate stdout
if [ -n "$file" ]; then
    exec 3> "$file"
else
    exec 3>&1
fi

# Rest of the program here.
# If there are input files (for example) that follow the options, they
# will remain in the "$@" positional parameters.

